# BlockChyp Python SDK

## Developer Setup
Create a virtual env with the following commands
```
py -3 -m venv env
env\scripts\activate
```
Install requirements with
```
pip install -Ur requirements.txt
```
Add requirements with
```
pip freeze > requirements.txt
```
Build package with
```
python setup.py sdist
```